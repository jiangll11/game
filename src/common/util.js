export const formatPrice = n => {
    if (n) {
        return parseFloat(n).toFixed(2)
    } else {
        return '0.00'
    }
    // return Math.floor(n * 100) / 100
}

export const checkMobile = mobile => {
    var myreg = /^((1[3456789]{1})+\d{9})$/
    return myreg.test(mobile)
}

export const isAndroid = () => {
    const ua = navigator.userAgent
    if (ua && ua.toLowerCase().indexOf('android') > -1) {
        return true
    }
    return false
}

export const isIOS = () => {
    const ua = navigator.userAgent
    if (ua && ua.toLowerCase().indexOf('iphone') > -1) {
        return true
    }
    return false
}

export const setDocumentTitle = (title, store) => {
    console.warn('setDocumentTitle:', title)
    if (store) {
        store.commit('changeNavBarTitle', title)
    }
    if (isIOS()) { // 如果是ios系统的uiwebview，需要使用下面方式修改标题
        console.log('###isIOS')
        document.title = title
        var hackIframe = document.createElement('iframe')
            // test jiangll todo 这里有问题，地址要改成使用我们的地址，否则可能因为https安全被拦截访问
            // hackIframe.src = 'https://www.baidu.com/favicon.ico'
        hackIframe.src = '/favicon.ico'
        hackIframe.style.display = 'none'
        hackIframe.addEventListener('load', function() {
            console.warn('iframe.addEventListener')
            setTimeout(function() {
                document.body.removeChild(hackIframe)
                hackIframe = null
            }, 10)
        })
        document.body.appendChild(hackIframe)
    } else {
        document.title = title
    }
    // 如果是 iOS 设备，则使用如下 hack 的写法实现页面标题的更新
    // if (navigator.userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/)) {
    //   console.log('###ios device')
    //   const hackIframe = document.createElement('iframe')
    //   hackIframe.style.display = 'none'
    //   hackIframe.src = process.env.BASE_URL + 'fixiostitle.html?r=' + Math.random()
    //   document.body.appendChild(hackIframe)
    //   setTimeout(() => {
    //     document.body.removeChild(hackIframe)
    //   }, 300)
    // }
}

export const getAssetsBasePageUrl = () => {
    var loc = location.href
    var base = loc.split('#')[0]
    return base
        // return location.protocol + '//' + location.host
}