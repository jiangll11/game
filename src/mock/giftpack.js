let packlist = [{
        "img": "/images/gift_banner1.jpg",
        "url": "https://mp.weixin.qq.com/s/9y_AlRsXUvTQpW0f6tj7mA",
        "type": 1
    },
    {
        "img": "/images/gift_banner2.jpg",
        "url": "https://mp.weixin.qq.com/s/OvNiagu_XZj3bjxVtwoPYw",
        "type": 1
    },
    {
        "img": "/images/gift_banner3.jpg",
        "url": "https://mp.weixin.qq.com/s/Hx4KKo3CTLUI_UOsPPe6UQ",
        "type": 1
    },
    {
        "img": "/images/gift_banner4.jpg",
        "url": "https://mp.weixin.qq.com/s/Ofa9xz_rMeHox_C6A2ce0g",
        "type": 1
    },
    {
        "img": "/images/gift_banner5.jpg",
        "url": "https://mp.weixin.qq.com/s/Tid3nI1MlDjfKG61Z9ONoQ",
        "type": 1
    },
    {
        "img": "/images/gift_banner6.jpg",
        "url": "https://mp.weixin.qq.com/s/-haezEt8jCD2HOYNMnyilg",
        "type": 1
    },
    {
        "img": "/images/gift_banner7.jpg",
        "url": "https://www.baidu.com",
        "type": 1
    },
]

export default {
    'get|/getgiftpack': () => {
        let packIndex = parseInt(Math.floor(Math.random() * 7))
            // let packIndex = 7
        console.log('packIndex:', packIndex)
        return {
            status: 200,
            message: 'success',
            data: packlist[packIndex]
        };
    }
}