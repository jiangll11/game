'use strict'
import { getAssetsBasePageUrl } from '../common/util.js'

var hostV
var socketUrlV
    // var hostV = process.env.NODE_ENV === 'development' ? 'apis/h5v2/h5api/api/h5v2' : 'http://222.73.47.23/H5v2/h5api/api/h5v2'
var productEnv = false

const apiHostConfig = [{ // 正式环境
        host: 'https://member.alading.com/h5v2/',
        apiUrl: 'https://member.alading.com/h5v2/h5v2api/api/h5v2',
        apiUrlDev: '/apis/h5v2/h5v2api/api/h5v2',
        socketUrl: 'wss://member.alading.com:8003/'
            // socketUrl: 'ws://alading.avantouch.com:8001/'
    },
    { // 测试环境
        host: 'http://192.168.10.39:3000',
        apiUrl: 'http://192.168.10.39:3000/v1',
        apiUrlDev: '/apis/v1',
        socketUrl: 'wss://payment2.alading.com:8003/'
    },
]

const baseUrl = getAssetsBasePageUrl().toLowerCase()
console.log('baseUrl:', baseUrl)
if (baseUrl) {
    for (var hostItem of apiHostConfig) {
        if (baseUrl.indexOf(hostItem.host) > -1) {
            hostV = process.env.NODE_ENV === 'development' ? hostItem.apiUrlDev : hostItem.apiUrl
            socketUrlV = hostItem.socketUrl
            break
        }
    }
}
if (hostV === apiHostConfig[0].apiUrl) {
    productEnv = true
}
export const isProductEnv = productEnv
    // console.log('hostV####:', hostV)
if (!hostV) {
    hostV = apiHostConfig[1].apiUrlDev
    socketUrlV = apiHostConfig[1].socketUrl
}

// export const websocketUrl = process.env.NODE_ENV === 'development' ? 'ws://222.73.47.23:8002/' : 'ws://222.73.47.23:8002/'
// export const websocketUrl = process.env.NODE_ENV === 'development' ? 'ws://222.73.47.23:8002/' : 'ws://alading.avantouch.com:8001/'

// hostV = 'http://58.33.45.75:3000/v1'
// hostV = 'http://192.168.10.39:3000/v1'
// hostV = '/apis/v1'
export const host = hostV
export const websocketUrl = socketUrlV
    // test jiangll 是否模拟
export const isMock = true

const apis = {
    'alading.giftpack': { // 获得红包
        url: '/getgiftpack',
        method: 'get',
        isMock: true
    },
}

export const apiList = apis

// 不需要token的接口
// export const authFreeApis = [
//   'alading.user.login', // 登录
//   'alading.user.home', // 首页商品列表
//   'alading.user.productDetail', // 商品详情
//   'alading.order.orderDetail.authfree' // 订单详情接口，如果是Param参数的不需要token
// ]

export const getUrlConfig = function(api) {
    if (apis[api]) {
        return apis[api]
            // return host + apis[api]
    }
    console.error('method:' + api + ' 找不到接口url定义')
}