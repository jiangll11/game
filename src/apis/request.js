'use strict'
import { isIOS } from '../common/util.js'
import { host, getUrlConfig } from './api-config.js'
import axios from 'axios'
// import { storage, storageKeys } from '../store/storage.js'

// axios.defaults.baseURL = host
const NETWORK_ERROR = '网络请求错误'

var request = {}
var CancelToken = axios.CancelToken

function onCatchError(reject, error, isCancel) {
    console.log('onCatchError:', isCancel, error)
    error = error || NETWORK_ERROR
    if (!isCancel) {
        if (isIOS()) {
            setTimeout(() => {
                reject(new Error(error))
            }, 350)
            return
        }
    }
    reject(new Error(error))
}

// cancelContext 在分页请求页面，为了避免下滑刷新和上拉加载更多冲突，需要cancelContext来取消前面的请求
request.send = function(urlKey, routeParam, params, data, cancelContext) {
    console.log('request.send1:', urlKey, routeParam, params, data)
    return new Promise((resolve, reject) => {
        // h5项目不要去账号登录
        // if (!checkAndLogin(urlKey)) {
        //   // 避免过快返回错误，导致页面上loading不能隐藏
        //   setTimeout(() => onCatchError(reject, getLoginErrorTip()), 200)
        //     // onCatchError(reject, getLoginErrorTip())
        //     return
        // }
        const urlConfig = getUrlConfig(urlKey)
        console.log('request.send2:', urlConfig)
        if (!urlConfig) {
            // 避免过快返回错误，导致页面上loading不能隐藏
            setTimeout(() => onCatchError(reject, '接口未定义'), 200)
                // onCatchError(reject, '接口未定义')
            return
        }
        var url
        if (urlConfig.isMock) {
            url = urlConfig.url
        } else {
            url = host + urlConfig.url
        }
        if (routeParam) {
            url += `/${routeParam}`
        }
        data = data || {}
        params = params || {}
        params.timestamp = new Date().getTime() + ''
            // 请求失败，重试3次逻辑
            // if (params.altimes) {
            //   params.altimes++
            // } else {
            //   params.altimes = 1
            // }
            // if (params.altimes >= 3) {
            //   console.log('服务请求超过限制次数')
            //   onCatchError(reject, NETWORK_ERROR)
            //   return
            // }
        var header = {
            // app: 'OKPay'
        }
        if (urlConfig.method !== 'get') {
            header['Content-type'] = 'application/json'
        }
        // const token = storage.getItem(storageKeys.token)
        // if (authFreeApis.indexOf(urlKey) < 0 || token) {
        //     header.token = token;
        //     // 更新token有效期
        //     storage.setItem(storageKeys.token, token)
        // }
        console.log('request.send32:', url, ' params:', params, ' data:', data, ' headers:', header)
        axios({
            url: url,
            method: urlConfig.method,
            params: params,
            headers: header,
            // timeout: 6000, // 超时设置
            data: data,
            cancelToken: new CancelToken(function(cancel) {
                if (cancelContext) {
                    cancelContext.cancel = cancel
                }
            })
        }).then(function(res) {
            console.log('request.send4 then:', res)
            if (res && res.status === 200) {
                if (res.data) {
                    // setTimeout(() => {
                    //         resolve(res.data)
                    //     }, 3000)
                    resolve(res.data)
                        // if (res.data.rsp_code == '00') {
                        //     resolve(res.data.data)
                        // } else {
                        //     onCatchError(reject, res.data.msg)
                        // }
                    return
                }
            }
            console.log('request.send then error')
            onCatchError(reject, NETWORK_ERROR)
        }).catch(function(err) {
            console.error('request.send5 catch:', err)
            if (axios.isCancel(err) || (err && err.message === 'aldcancel')) {
                console.error('request.send6 catch is cancel')
                onCatchError(reject, NETWORK_ERROR, true)
            } else {
                onCatchError(reject, NETWORK_ERROR)
            }
        })
    })
}

export default request