const externals = {
    vue: 'Vue',
    axios: 'axios',
    'VConsole': 'VConsole',
    // 'fastclick': 'FastClick',
}

const cdn = {
    js: [
        './cdn/vue2.6.11.min.js',
        './cdn/axios0.19.2.min.js',
        './cdn/vconsole3.3.4.min.js',
    ]
}

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? '' : '',
    // publicPath: process.env.NODE_ENV === 'production'
    //   ? '/production-sub-path/'
    //   : '',
    lintOnSave: false,
    productionSourceMap: false, // 生产环境的 source map
    // filenameHashing: true,
    chainWebpack: config => {
        config.plugin('html').tap(args => {
            args[0].title = 'h5game template'
            if (process.env.NODE_ENV === 'production') {
                args[0].cdn = cdn
            }
            if (process.env.NODE_ENV === 'development') {
                args[0].cdn = cdn
            }
            return args
        })
        config.optimization
            .splitChunks({
                // chunks: 'all',
                // minSize: 30000,
                // maxSize: 0,
                // minChunks: 2,
                // maxAsyncRequests: 5,
                // maxInitialRequests: 3,
                // automaticNameDelimiter: '~',
                // name: true,
                cacheGroups: {
                    // vendors: {
                    //   test: /[\\/]node_modules[\\/]/,
                    //   priority: -10
                    // },
                    // default: {
                    //   minChunks: 2,
                    //   priority: -20,
                    //   reuseExistingChunk: true
                    // },
                    // vendors: {
                    //   reuseExistingChunk: true
                    // },
                    // vendors: {
                    //   test: /[\\/]node_modules[\\/]/,
                    //   name: 'vendors',
                    //   chunks: 'all'
                    // },
                    commons: {
                        name: "commons",
                        chunks: "all",
                        minChunks: 2
                    }
                }
            })
    },
    configureWebpack: config => {
        config.externals = externals
        const plugins = []
        if (process.env.NODE_ENV === 'production') {
            plugins.push(new BundleAnalyzerPlugin())
                // plugins.push(new CompressionWebpackPlugin({
                //   filename: '[path].gz[query]',
                //   algorithm: 'gzip',
                //   test: productionGzipExtensions,
                //   threshold: 10240,
                //   minRatio: 0.8
                // }))
        }
        config.performance = {
            // false | "error" | "warning" // 不显示性能提示 | 以错误形式提示 | 以警告...
            hints: "warning",
            // 开发环境设置较大防止警告
            // 根据入口起点的最大体积，控制webpack何时生成性能提示,整数类型,以字节为单位
            maxEntrypointSize: 5000000,
            // 最大单个资源体积，默认250000 (bytes)
            maxAssetSize: 3000000
        }
        config.plugins = [...config.plugins, ...plugins]
    },
    css: {
        loaderOptions: {
            // 给 sass-loader 传递选项
            sass: {
                // @/ 是 src/ 的别名
                // 所以这里假设你有 `src/variables.scss` 这个文件
                // data: `@import "~@/scss/index.scss";`
                // prependData: `@import "~@/scss/index.scss";`
            }
        }
    },
    devServer: {
        // open: true,
        // proxy: 'https://member.alading.com/'
        proxy: {
            '/apis': {
                target: 'https://payment2.alading.com/',
                // target: 'https://member.alading.com/',
                // target: 'http://test.finalshock.com.cn/',
                // target: 'http://222.73.47.23/',
                changeOrigin: true,
                pathRewrite: {
                    '^/apis': ''
                }
            }
        },
        https: false
    }
}